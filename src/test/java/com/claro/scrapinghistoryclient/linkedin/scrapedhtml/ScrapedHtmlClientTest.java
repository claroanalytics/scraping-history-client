package com.claro.scrapinghistoryclient.linkedin.scrapedhtml;

import com.claro.scrapinghistoryclient.linkedin.scrapedhtml.dto.HistoryHtmlResponse;
import com.claro.scrapinghistoryclient.linkedin.scrapedhtml.dto.HtmlToSaveRequest;
import com.claro.scrapinghistoryclient.linkedin.scrapedhtml.dto.HtmlToSaveResponse;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import java.util.Date;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class ScrapedHtmlClientTest {

    String endpointGetById = "http://localhost:8080/linkedin/scrapedHtml/byId/";
    String endpointSave = "http://localhost:8080/linkedin/scrapedHtml/";

    ScrapedHtmlClient onTest;
    @Mock
    RestTemplate restTemplate;

    @BeforeEach
    void setup() {
        onTest = getScrapedHtmlClient();
        mockRoutes(onTest);
    }

    private void mockRoutes(ScrapedHtmlClient onTest) {
        ReflectionTestUtils.setField(onTest,
                "scrapedHistoryClientUrl", "http://localhost:8080");
        ReflectionTestUtils.setField(onTest,
                "scrapedHtmlEndpoint", "/linkedin/scrapedHtml");
        ReflectionTestUtils.setField(onTest,
                "getByIdEndpoint", "/byId");
    }

    @Test
    void testGetById() {
        String id = "6183b3fd544edc70ea99803a";
        HistoryHtmlResponse response = new HistoryHtmlResponse(
                "id", "html", "url", new Date());
        when(restTemplate.getForEntity(endpointGetById + id,
                HistoryHtmlResponse.class))
                .thenReturn(ResponseEntity.ok(response));

        HistoryHtmlResponse result = onTest.getById(id).get();

        assertThat(result).isEqualTo(response);
    }

    @Test
    void testGetByIdNotFound() {
        String id = "6183b3fd544edc70ea99803a";
        HistoryHtmlResponse response = new HistoryHtmlResponse(
                "id", "html", "url", new Date());
        when(restTemplate.getForEntity(endpointGetById + id,
                HistoryHtmlResponse.class))
                .thenThrow(new HttpClientErrorException(HttpStatus.NOT_FOUND));

        Optional<HistoryHtmlResponse> result = onTest.getById(id);

        assertThat(result.isPresent()).isFalse();
    }

    @Test
    void testExceptionIfNoUrlProvided() {
        String exceptionMessage = "Service url not defined!";
        onTest = getScrapedHtmlClient();
        assertThatThrownBy(() -> onTest.getById("id"))
                .hasMessage(exceptionMessage);
        assertThatThrownBy(() -> onTest.getById(""))
                .hasMessage(exceptionMessage);
    }

    private ScrapedHtmlClient getScrapedHtmlClient() {
        return new ScrapedHtmlClient(restTemplate);
    }

}
