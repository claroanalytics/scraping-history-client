package com.claro.scrapinghistoryclient.linkedin.historyrecord;

import com.claro.scrapinghistoryclient.linkedin.config.HistoryRecordConfig;
import com.claro.scrapinghistoryclient.linkedin.historyrecord.dto.HistoryRecordResponse;
import com.claro.scrapinghistoryclient.linkedin.historyrecord.dto.historyrecord.HistoryRecord;
import com.claro.scrapinghistoryclient.linkedin.scrapedhtml.dto.HtmlToSaveRequest;
import com.claro.scrapinghistoryclient.linkedin.scrapedhtml.dto.HtmlToSaveResponse;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Import;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.reactive.function.client.WebClient;

import java.util.Date;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;


@Import(HistoryRecordConfig.class)
@ExtendWith(MockitoExtension.class)
class HistoryRecordClientImplTest {

    HistoryRecordClientImpl onTest;

    @Autowired
    private WebClient webClient;

    @Mock
    RestTemplate restTemplate;

    @BeforeEach
    void setup() {
        onTest = getHistoryRecordClient();
        mockRoutes(onTest);
    }

    private void mockRoutes(HistoryRecordClientImpl onTest) {
        ReflectionTestUtils.setField(onTest,
                "scrapedHistoryClientUrl", "http://localhost:8080");
        ReflectionTestUtils.setField(onTest,
                "historyRecordEndpoint", "/linkedin/historyRecord");
        ReflectionTestUtils.setField(onTest,
                "createHistoryEndpoint", "/create");
        ReflectionTestUtils.setField(onTest,
                "getByQueryEndpoint", "/");
        ReflectionTestUtils.setField(onTest,
                "getByIdEndpoint", "/byId");
        ReflectionTestUtils.setField(onTest,
                "parseEndpoint", "/parse");
        ReflectionTestUtils.setField(onTest,
                "appendLastEndpoint", "/appendLast");
    }

    @Test
    void testCreateNewHistory() {
        HtmlToSaveRequest request = new HtmlToSaveRequest(
                "TEST URL", "html", new Date());
        HtmlToSaveResponse response = new HtmlToSaveResponse("id",
                request.getUrl());
        when(restTemplate.postForEntity(anyString(),
                eq(new HttpEntity<>(request)),
                eq(HtmlToSaveResponse.class)))
                .thenReturn(ResponseEntity.ok(response));

        HtmlToSaveResponse result = onTest.createHistoryFromHtml(request);

        assertThat(result.getUrl()).isEqualTo(result.getUrl());
        assertThat(result.getId()).isNotNull();
    }

    @Test
    void testGetById() {
        String id = "id";
        HistoryRecordResponse response = new HistoryRecordResponse(
                "htmlId", "id", "url", new HistoryRecord(), new Date(), null);
        when(restTemplate.getForEntity(anyString(), any()))
                .thenReturn(ResponseEntity.ok(response));

        HistoryRecordResponse result = onTest.getById(id).get();

        assertThat(result).isEqualTo(response);
    }

    private HistoryRecordClientImpl getHistoryRecordClient() {
        return new HistoryRecordClientImpl(restTemplate, webClient);
    }

}
