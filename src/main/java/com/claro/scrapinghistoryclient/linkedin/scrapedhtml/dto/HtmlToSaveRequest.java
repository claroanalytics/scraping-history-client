package com.claro.scrapinghistoryclient.linkedin.scrapedhtml.dto;

import lombok.Builder;
import lombok.Value;

import java.util.Date;

@Value
public class HtmlToSaveRequest {
    String url;
    String html;
    Date dateScraped;
}
