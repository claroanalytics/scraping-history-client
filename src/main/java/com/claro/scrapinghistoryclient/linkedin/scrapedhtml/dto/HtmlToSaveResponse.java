package com.claro.scrapinghistoryclient.linkedin.scrapedhtml.dto;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.Value;


@Value
@NoArgsConstructor(force = true, access = AccessLevel.PRIVATE)
@AllArgsConstructor
public class HtmlToSaveResponse {
    String id;
    String url;
}
