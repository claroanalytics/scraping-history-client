package com.claro.scrapinghistoryclient.linkedin.scrapedhtml.dto;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.Value;

import java.util.Date;

@Value
@NoArgsConstructor(force = true, access = AccessLevel.PRIVATE)
@AllArgsConstructor
public class HistoryHtmlResponse {
    String id;
    String html;
    String url;
    Date addedTime;
}
