package com.claro.scrapinghistoryclient.linkedin.scrapedhtml;


import com.claro.scrapinghistoryclient.linkedin.scrapedhtml.dto.HistoryHtmlResponse;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import java.util.Optional;

@RequiredArgsConstructor
@Service
public class ScrapedHtmlClient {

    @Value("${scrapedHistoryClient.url:#{null}}")
    private String  scrapedHistoryClientUrl;

    @Value("${scrapedHistoryClient.scrapedHtmlEndpoint.url:/linkedin/scrapedHtml}")
    private String scrapedHtmlEndpoint;

    @Value("${scrapedHistoryClient.scrapedHtmlEndpoint.getByIdEndpoint:/byId}")
    private String getByIdEndpoint;

    private final RestTemplate restTemplate;

    public Optional<HistoryHtmlResponse> getById(String id) {
        checkFieldsSet();
        String url = scrapedHistoryClientUrl + scrapedHtmlEndpoint
                + getByIdEndpoint + "/" + id;
        try {
            ResponseEntity<HistoryHtmlResponse> responseEntity = restTemplate
                    .getForEntity(url, HistoryHtmlResponse.class);
            return Optional.ofNullable(responseEntity.getBody());
        } catch (HttpClientErrorException e) {
            if (e.getStatusCode() == HttpStatus.NOT_FOUND) {
                return Optional.empty();
            }
            throw e;
        }
    }

    private void checkFieldsSet() {
        if (scrapedHistoryClientUrl == null) {
            throw new UnsupportedOperationException("Service url not defined!");
        }
    }
}
