package com.claro.scrapinghistoryclient.linkedin.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.reactive.function.client.WebClient;

@Configuration
public class HistoryRecordConfig {

    @Bean
    public WebClient buildWebClient(
            @Value("${scrapedHistoryClient.url:#{null}}")
            final String clientUrl,
            @Value("${scrapedHistoryClient.historyRecordServiceUrl.url:/linkedin/historyRecord}")
            final String historyRecordEndpoint) {
        return WebClient.create(String.format("%s%s", clientUrl, historyRecordEndpoint));
    }

}
