package com.claro.scrapinghistoryclient.linkedin.historyrecord.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Getter
public class HistoryAppendingResponse {
    String newHistoryId;
}
