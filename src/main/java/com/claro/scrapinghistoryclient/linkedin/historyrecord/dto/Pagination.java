package com.claro.scrapinghistoryclient.linkedin.historyrecord.dto;

import lombok.Value;

@Value
public class Pagination {
    int skipped;
    int countPerPage;
}
