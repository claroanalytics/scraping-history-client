package com.claro.scrapinghistoryclient.linkedin.historyrecord.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;

import java.util.Date;
import java.util.Set;

@Builder
@Getter
@AllArgsConstructor
public class HistoryQuery {
    private final String url;
    private final Date dateFrom;
    private final Date dateTo;
    private final DateSorted dateSorted;
    private final Set<String> excludedIds;

    public enum DateSorted { ASC, DESC }
}
