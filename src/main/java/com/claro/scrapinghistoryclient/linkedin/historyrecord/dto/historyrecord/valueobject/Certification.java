package com.claro.scrapinghistoryclient.linkedin.historyrecord.dto.historyrecord.valueobject;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Certification {
    @JsonProperty("title")
    String title;
    @JsonProperty("organization")
    String organization;
    @JsonProperty("period")
    Period period;
}
