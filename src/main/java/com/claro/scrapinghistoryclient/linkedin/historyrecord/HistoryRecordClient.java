package com.claro.scrapinghistoryclient.linkedin.historyrecord;

import com.claro.scrapinghistoryclient.linkedin.historyrecord.dto.HistoryQuery;
import com.claro.scrapinghistoryclient.linkedin.historyrecord.dto.HistoryRecordResponse;
import com.claro.scrapinghistoryclient.linkedin.historyrecord.dto.Pagination;
import com.claro.scrapinghistoryclient.linkedin.historyrecord.dto.historyrecord.HistoryRecord;
import com.claro.scrapinghistoryclient.linkedin.scrapedhtml.dto.HtmlToSaveRequest;
import com.claro.scrapinghistoryclient.linkedin.scrapedhtml.dto.HtmlToSaveResponse;
import reactor.core.publisher.Flux;

import java.util.List;
import java.util.Optional;

public interface HistoryRecordClient {
    /**
     * Saves and processes history for scraped html.
     * @param request Request
     * @return Created history response
     */
    HtmlToSaveResponse createHistoryFromHtml(HtmlToSaveRequest request);

    /**
     * Gets list of history by query
     * @param query Query
     * @return flux of history
     */
    Flux<HistoryRecordResponse> getByQuery(HistoryQuery query);

    /**
     * Gets history by id
     * @param id History id
     * @return History record
     */
    Optional<HistoryRecordResponse> getById(String id);

    /**
     * Queries for history with pagination
     * @param query History query
     * @param pagination Pagination
     * @return Flux of history
     */
    Flux<HistoryRecordResponse> getPage(HistoryQuery query, Pagination pagination);

    /**
     * Tries to parse html by history parser.
     * @param html Html
     * @return Optional of parsed history record
     */
    Optional<HistoryRecord> parse(String html);

    /**
     * Appends some data to last saved history found by url.
     * @param url Url
     * @param toAppend Data to append (
     * fields that are not desired to be changed must be null)
     * @return Id of appended history
     */
    String appendLast(String url, HistoryRecord toAppend);
}
