package com.claro.scrapinghistoryclient.linkedin.historyrecord.dto.historyrecord.valueobject;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Experience {
    @JsonProperty("title")
    String title;
    @JsonProperty("company")
    String company;
    @JsonProperty("companyUrl")
    String companyUrl;
    @JsonProperty("period")
    Period period;
    @JsonProperty("description")
    String description;
    @JsonProperty("location")
    String location;
}
