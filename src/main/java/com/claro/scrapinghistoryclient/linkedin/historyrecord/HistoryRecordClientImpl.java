package com.claro.scrapinghistoryclient.linkedin.historyrecord;

import com.claro.scrapinghistoryclient.linkedin.historyrecord.dto.HistoryAppendingResponse;
import com.claro.scrapinghistoryclient.linkedin.historyrecord.dto.HistoryQuery;
import com.claro.scrapinghistoryclient.linkedin.historyrecord.dto.HistoryRecordResponse;
import com.claro.scrapinghistoryclient.linkedin.historyrecord.dto.Pagination;
import com.claro.scrapinghistoryclient.linkedin.historyrecord.dto.historyrecord.HistoryRecord;
import com.claro.scrapinghistoryclient.linkedin.scrapedhtml.dto.HtmlToSaveRequest;
import com.claro.scrapinghistoryclient.linkedin.scrapedhtml.dto.HtmlToSaveResponse;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Flux;

import java.util.Optional;

@Service
@RequiredArgsConstructor
public class HistoryRecordClientImpl implements HistoryRecordClient {
    @Value("${scrapedHistoryClient.url:#{null}}")
    private String scrapedHistoryClientUrl;

    @Value("${scrapedHistoryClient.historyRecordServiceUrl.url:/linkedin/historyRecord}")
    private String historyRecordEndpoint;

    @Value("${scrapedHistoryClient.historyRecordServiceUrl.createHistory:/create}")
    private String createHistoryEndpoint;

    @Value("${scrapedHistoryClient.historyRecordServiceUrl.getByQueryEndpoint:/}")
    private String getByQueryEndpoint;

    @Value("${scrapedHistoryClient.historyRecordServiceUrl.getByIdEndpoint:/byId}")
    private String getByIdEndpoint;

    @Value("${scrapedHistoryClient.historyRecordServiceUrl.parseEndpoint:/parse}")
    private String parseEndpoint;

    @Value("${scrapedHistoryClient.historyRecordServiceUrl.parseEndpoint:/appendLast}")
    private String appendLastEndpoint;

    private final RestTemplate restTemplate;

    private final WebClient webClient;

    public HtmlToSaveResponse createHistoryFromHtml(HtmlToSaveRequest request) {
        checkFieldsSet();
        String url = scrapedHistoryClientUrl + historyRecordEndpoint
                + createHistoryEndpoint;
        HttpEntity<HtmlToSaveRequest> entity = new HttpEntity<>(request);
        ResponseEntity<HtmlToSaveResponse> response = restTemplate
                .postForEntity(url, entity, HtmlToSaveResponse.class);
        return response.getBody();
    }

    public Flux<HistoryRecordResponse> getByQuery(HistoryQuery query) {
        return webClient.post()
                .uri(getByQueryEndpoint)
                .bodyValue(query)
                .accept(MediaType.APPLICATION_STREAM_JSON)
                .exchangeToFlux(clientResponse -> clientResponse
                        .bodyToFlux(HistoryRecordResponse.class))
                .limitRate(50);
    }

    public Optional<HistoryRecordResponse> getById(String id) {
        checkFieldsSet();
        String url = scrapedHistoryClientUrl
                + historyRecordEndpoint + getByIdEndpoint + "/" + id;
        try {
            HistoryRecordResponse response = restTemplate
                    .getForEntity(url, HistoryRecordResponse.class)
                    .getBody();
            return Optional.ofNullable(response);
        } catch (HttpClientErrorException e) {
            if (e.getStatusCode() == HttpStatus.NOT_FOUND) {
                return Optional.empty();
            }
            throw e;
        }
    }

    public Flux<HistoryRecordResponse> getPage(HistoryQuery query, Pagination pagination) {
        PageableHistoryQuery pageableHistoryQuery = PageableHistoryQuery
                .pageableBuilder().historyQuery(query).pagination(pagination).build();
        return getByQuery(pageableHistoryQuery);
    }

    public Optional<HistoryRecord> parse(String html) {
        String url = scrapedHistoryClientUrl
                + historyRecordEndpoint + parseEndpoint;
        try {
            ResponseEntity<HistoryRecord> responseEntity = restTemplate
                    .postForEntity(url, html, HistoryRecord.class);
            return Optional.ofNullable(responseEntity.getBody());
        } catch (HttpClientErrorException e) {
            if (e.getStatusCode() == HttpStatus.UNPROCESSABLE_ENTITY) {
                return Optional.empty();
            }
            throw e;
        }
    }

    public String appendLast(String url, HistoryRecord toAppend) {
        String apiUrl = scrapedHistoryClientUrl
                + historyRecordEndpoint + appendLastEndpoint;

        AppendLastRequest request = new AppendLastRequest(url, toAppend);
        HttpEntity<AppendLastRequest> httpEntity = new HttpEntity<>(request);
        return restTemplate
                .postForEntity(apiUrl, httpEntity, HistoryAppendingResponse.class)
                .getBody()
                .getNewHistoryId();
    }


    private void checkFieldsSet() {
        if (scrapedHistoryClientUrl == null) {
            throw new UnsupportedOperationException("Service url not defined!");
        }
    }

    @Getter
    private static class PageableHistoryQuery extends HistoryQuery {
        private final Pagination pagination;

        @Builder(builderMethodName = "pageableBuilder")
        PageableHistoryQuery(HistoryQuery historyQuery, Pagination pagination) {
            super(historyQuery.getUrl(), historyQuery.getDateFrom(),
                    historyQuery.getDateTo(), historyQuery.getDateSorted(),
                    historyQuery.getExcludedIds());
            this.pagination = pagination;
        }
    }

    @AllArgsConstructor
    @Getter
    private static class AppendLastRequest {
        String url;
        HistoryRecord historyRecord;
    }
}
