package com.claro.scrapinghistoryclient.linkedin.historyrecord.dto.historyrecord.valueobject;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Opportunities {
    /** True if profile status is "hiring". */
    @JsonProperty("isHiring")
    boolean isHiring;

    /** True if profile status is "Open to work". */
    @JsonProperty("isOpenToWork")
    boolean isOpenToWork;
}
