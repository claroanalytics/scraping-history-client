package com.claro.scrapinghistoryclient.linkedin.historyrecord.dto;

import com.claro.scrapinghistoryclient.linkedin.historyrecord.dto.historyrecord.HistoryRecord;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.Value;

import java.util.Date;

@Value
@NoArgsConstructor(force = true, access = AccessLevel.PRIVATE)
@AllArgsConstructor
public class HistoryRecordResponse {
    String htmlId;
    String id;
    String url;
    HistoryRecord historyRecord;
    Date addedTime;
    String sourceName;
}
