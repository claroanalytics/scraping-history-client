package com.claro.scrapinghistoryclient.linkedin.historyrecord.dto.historyrecord.valueobject;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

/**
 * Publication ignore old fields.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@Data
public class Publication {

    @JsonProperty("title")
    private String title;
    @JsonProperty("date")
    private String date;
    @JsonProperty("attribution")
    private String attribution;
    @JsonProperty("summary")
    private String summary;
    @JsonProperty("subtitle")
    private String subtitle;
    @JsonProperty("authors")
    private String authors;

    @JsonProperty("title")
    public String getTitle() {
        return title;
    }

    @JsonProperty("title")
    public void setTitle(String title) {
        this.title = title;
    }

    @JsonProperty("date")
    public String getDate() {
        return date;
    }

    @JsonProperty("date")
    public void setDate(String date) {
        this.date = date;
    }

    @JsonProperty("attribution")
    public String getAttribution() {
        return attribution;
    }

    @JsonProperty("attribution")
    public void setAttribution(String attribution) {
        this.attribution = attribution;
    }

    @JsonProperty("summary")
    public String getSummary() {
        return summary;
    }

    @JsonProperty("summary")
    public void setSummary(String summary) {
        this.summary = summary;
    }

    @JsonProperty("subtitle")
    public String getSubtitle() {
        return subtitle;
    }
    
    @JsonProperty("subtitle")
    public void setSubtitle(String subtitle) {
        this.subtitle = subtitle;
    }

    /**
     * Getter.
     *
     * @return authors.
     */
    @JsonProperty("authors")
    public String getAuthors() {
        return authors;
    }

    /**
     * Setter.
     *
     * @param authorsArg authors.
     */
    @JsonProperty("authors")
    public void setAuthors(final String authorsArg) {
        authors = authorsArg;
    }
}
