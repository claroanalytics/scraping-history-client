package com.claro.scrapinghistoryclient.linkedin.historyrecord.dto.historyrecord.valueobject;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Education {
    @JsonProperty("school")
    String school;
    @JsonProperty("details")
    String details;
    @JsonProperty("period")
    Period period;
    @JsonProperty("description")
    String description;
}
