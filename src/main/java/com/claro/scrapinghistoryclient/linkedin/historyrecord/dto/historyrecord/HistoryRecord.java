package com.claro.scrapinghistoryclient.linkedin.historyrecord.dto.historyrecord;

import com.claro.scrapinghistoryclient.linkedin.historyrecord.dto.historyrecord.valueobject.*;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

/**
 * Extractor snapshot structure.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class HistoryRecord {
    /**
     * Name.
     */
    @JsonProperty("name")
    private String name;
    /**
     * Picture.
     */
    @JsonProperty("picture")
    private String picture;
    /**
     * Posittion title.
     */
    @JsonProperty("position_title")
    private String positionTitle;
    /**
     * Current company.
     */
    @JsonProperty("current_company")
    private String currentCompany;
    /**
     * Surname.
     */
    @JsonProperty("surname")
    private String surname;
    /**
     * Location.
     */
    @JsonProperty("location")
    private String location;
    /**
     * Interests.
     */
    @JsonProperty("interests")
    private String interests;

    /**
     * Profile "Opportunities" statuses.
     */
    @JsonProperty("opportunities")
    private Opportunities opportunities;
    /**
     * Recommendation count.
     */
    @JsonProperty("recommendation_count")
    private String recommendationCount;
    /**
     * Connections.
     */
    @JsonProperty("connections")
    private String connections;
    /**
     * Summary.
     */
    @JsonProperty("summary")
    private String summary;
    /**
     * Headline.
     */
    @JsonProperty("headline")
    private String headline;
    /**
     * Skills.
     */
    @JsonProperty("skills")
    private List<String> skills = null;
    /**
     * Contact for.
     */
    @JsonProperty("contact_for")
    private List<Object> contactFor = null;
    /**
     * Education.
     */
    @JsonProperty("education")
    private List<Education> education = null;
    /**
     * Experience.
     */
    @JsonProperty("experience")
    private List<Experience> experience = null;
    /**
     * Groups.
     */
    @JsonProperty("groups")
    private List<String> groups = null;
    /**
     * Websites.
     */
    @JsonProperty("websites")
    private List<Object> websites = null;
    /**
     * Projects.
     */
    @JsonProperty("projects")
    private List<Object> projects = null;
    /**
     * Courses.
     */
    @JsonProperty("courses")
    private List<String> courses = null;
    /**
     * Languages.
     */
    @JsonProperty("languages")
    private List<String> languages = null;
    /**
     * Publications.
     */
    @JsonProperty("publications")
    private List<Publication> publications = null;
    /**
     * Industry.
     */
    @JsonProperty("industry")
    private String industry;
    /**
     * Version.
     */
    @JsonProperty("version")
    private String version;
    /**
     * Certifications.
     */
    @JsonProperty("certifications")
    private List<Certification> certifications = null;
    /**
     * Urls.
     */
    @JsonProperty("urls")
    private List<String> urls = null;

    @JsonProperty("isTakenUnderAuthorization")
    private boolean isTakenUnderAuthorization;

    @JsonProperty("isTakenUnderAuthorization")
    public void setIsTakenUnderAuthorization(boolean isTakenUnderAuthorization) {
        this.isTakenUnderAuthorization = isTakenUnderAuthorization;
    }

    @JsonProperty("isTakenUnderAuthorization")
    public boolean isTakenUnderAuthorization() {
        return this.isTakenUnderAuthorization;
    }

    /**
     * Getter.
     * @return string
     */
    @JsonProperty("name")
    public String getName() {
        return name;
    }

    /**
     * Setter.
     * @param nameArg name
     */
    @JsonProperty("name")
    public void setName(final String nameArg) {
        this.name = nameArg;
    }

    /**
     * Getter.
     * @return string
     */
    @JsonProperty("position_title")
    public String getPositionTitle() {
        return positionTitle;
    }

    /**
     * Setter.
     * @param positionTitleArg position title
     */
    @JsonProperty("position_title")
    public void setPositionTitle(final String positionTitleArg) {
        this.positionTitle = positionTitleArg;
    }

    /**
     * Getter.
     * @return string
     */
    @JsonProperty("current_company")
    public String getCurrentCompany() {
        return currentCompany;
    }

    /**
     * Setter.
     * @param currentCompanyArg current company
     */
    @JsonProperty("current_company")
    public void setCurrentCompany(final String currentCompanyArg) {
        this.currentCompany = currentCompanyArg;
    }

    /**
     * Setter.
     * @return string
     */
    @JsonProperty("surname")
    public String getSurname() {
        return surname;
    }

    /**
     * Getter.
     * @param surnameArg surname
     */
    @JsonProperty("surname")
    public void setSurname(final String surnameArg) {
        this.surname = surnameArg;
    }

    /**
     * Getter.
     * @return string
     */
    @JsonProperty("location")
    public String getLocation() {
        return location;
    }

    /**
     * Setter.
     * @param locationArg location
     */
    @JsonProperty("location")
    public void setLocation(final String locationArg) {
        this.location = locationArg;
    }

    /**
     * Getter.
     * @return string
     */
    @JsonProperty("interests")
    public String getInterests() {
        return interests;
    }

    /**
     * Getter.
     * @return Opportunities
     */
    @JsonProperty("opportunities")
    public Opportunities getOpportunities() {
        return opportunities;
    }

    /**
     * Setter.
     * @param opportunitiesArg Opportunities
     */
    @JsonProperty("opportunities")
    public void setOpportunities(
            final Opportunities opportunitiesArg) {
        opportunities = opportunitiesArg;
    }

    /**
     * Setter.
     * @param interestsArg interestsArg
     */
    @JsonProperty("interests")
    public void setInterests(final String interestsArg) {
        this.interests = interestsArg;
    }

    /**
     * Getter.
     * @return string
     */
    @JsonProperty("recommendation_count")
    public String getRecommendationCount() {
        return recommendationCount;
    }

    /**
     * Setter.
     * @param recommendationCountArg recommedation count
     */
    @JsonProperty("recommendation_count")
    public void setRecommendationCount(final String recommendationCountArg) {
        this.recommendationCount = recommendationCountArg;
    }

    /**
     * Getter.
     * @return string
     */
    @JsonProperty("connections")
    public String getConnections() {
        return connections;
    }

    /**
     * Setter.
     * @param connectionsArg connections
     */
    @JsonProperty("connections")
    public void setConnections(final String connectionsArg) {
        this.connections = connectionsArg;
    }

    /**
     * Getter.
     * @return string
     */
    @JsonProperty("summary")
    public String getSummary() {
        return summary;
    }

    /**
     * Setter.
     * @param summaryArg summary
     */
    @JsonProperty("summary")
    public void setSummary(final String summaryArg) {
        this.summary = summaryArg;
    }

    /**
     * Getter.
     * @return string
     */
    @JsonProperty("headline")
    public String getHeadline() {
        return headline;
    }

    /**
     * Setter.
     * @param headlineArg headline
     */
    @JsonProperty("headline")
    public void setHeadline(final String headlineArg) {
        this.headline = headlineArg;
    }

    /**
     * Getter.
     * @return list
     */
    @JsonProperty("skills")
    public List<String> getSkills() {
        return skills;
    }

    /**
     * Setter.
     * @param skillsArg skills
     */
    @JsonProperty("skills")
    public void setSkills(final List<String> skillsArg) {
        this.skills = skillsArg;
    }

    /**
     * Getter.
     *
     * @return courses.
     */
    public List<String> getCourses() {
        return courses;
    }

    /**
     * Setter.
     *
     * @param coursesArg courses.
     */
    public void setCourses(final List<String> coursesArg) {
        courses = coursesArg;
    }

    /**
     * Getter.
     * @return list
     */
    @JsonProperty("contact_for")
    public List<Object> getContactFor() {
        return contactFor;
    }

    /**
     * Setter.
     * @param contactForArg contact for
     */
    @JsonProperty("contact_for")
    public void setContactFor(final List<Object> contactForArg) {
        this.contactFor = contactForArg;
    }

    /**
     * Getter.
     * @return list
     */
    @JsonProperty("education")
    public List<Education> getEducation() {
        return education;
    }

    /**
     * Setter.
     * @param educationArg education
     */
    @JsonProperty("education")
    public void setEducation(final List<Education> educationArg) {
        this.education = educationArg;
    }

    /**
     * Getter.
     * @return list
     */
    @JsonProperty("experience")
    public List<Experience> getExperience() {
        return experience;
    }

    /**
     * Setter.
     * @param experienceArg experience
     */
    @JsonProperty("experience")
    public void setExperience(final List<Experience> experienceArg) {
        this.experience = experienceArg;
    }

    /**
     * Getter.
     * @return list
     */
    @JsonProperty("groups")
    public List<String> getGroups() {
        return groups;
    }

    /**
     * Setter.
     * @param groupsArg groups
     */
    @JsonProperty("groups")
    public void setGroups(final List<String> groupsArg) {
        this.groups = groupsArg;
    }

    /**
     * Getter.
     * @return list
     */
    @JsonProperty("websites")
    public List<Object> getWebsites() {
        return websites;
    }

    /**
     * Setter.
     * @param websitesArg website
     */
    @JsonProperty("websites")
    public void setWebsites(final List<Object> websitesArg) {
        this.websites = websitesArg;
    }

    /**
     * Getter.
     * @return list
     */
    @JsonProperty("projects")
    public List<Object> getProjects() {
        return projects;
    }

    /**
     * Setter.
     * @param projectsArg projects
     */
    @JsonProperty("projects")
    public void setProjects(final List<Object> projectsArg) {
        this.projects = projectsArg;
    }

    /**
     * Getter.
     * @return list
     */
    @JsonProperty("languages")
    public List<String> getLanguages() {
        return languages;
    }

    /**
     * Setter.
     * @param languagesArg languages
     */
    @JsonProperty("languages")
    public void setLanguages(final List<String> languagesArg) {
        this.languages = languagesArg;
    }

    /**
     * Getter.
     * @return list
     */
    @JsonProperty("publications")
    public List<Publication> getPublications() {
        return publications;
    }

    /**
     * Setter.
     * @param publicationsArg publications
     */
    @JsonProperty("publications")
    public void setPublications(final List<Publication> publicationsArg) {
        this.publications = publicationsArg;
    }

    /**
     * Getter.
     * @return string
     */
    @JsonProperty("industry")
    public String getIndustry() {
        return industry;
    }

    /**
     * Setter.
     * @param industryArg industry
     */
    @JsonProperty("industry")
    public void setIndustry(final String industryArg) {
        this.industry = industryArg;
    }

    /**
     * Getter.
     * @return string
     */
    @JsonProperty("version")
    public String getVersion() {
        return version;
    }

    /**
     * Setter.
     * @param versionArg version
     */
    @JsonProperty("version")
    public void setVersion(final String versionArg) {
        this.version = versionArg;
    }

    /**
     * Getter.
     * @return string
     */
    public String getPicture() {
        return picture;
    }

    /**
     * Setter.
     * @param pictureArg picture
     */
    public void setPicture(final String pictureArg) {
        this.picture = pictureArg;
    }

    /**
     * Getter.
     * @return list
     */
    @JsonProperty("certifications")
    public List<Certification> getCertifications() {
        return certifications;
    }

    /**
     * Setter.
     * @param certificationsArg certifications
     */
    @JsonProperty("certifications")
    public void setCertifications(final List<Certification> certificationsArg) {
        this.certifications = certificationsArg;
    }

    /**
     * Getter.
     * @return list
     */
    @JsonProperty("urls")
    public List<String> getUrls() {
        return urls;
    }

    /**
     * Setter.
     * @param urlsArg urls
     */
    @JsonProperty("urls")
    public void setUrls(final List<String> urlsArg) {
        this.urls = urlsArg;
    }
}


